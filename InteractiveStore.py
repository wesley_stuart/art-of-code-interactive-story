'''
Art-Of-Code
Challenge #2
Interactive Store
Created by: Wesley Stuart
'''
from AnimateText import TextAnimations
from Music import PlayMusic # Create a class just for the music library
from pygame import mixer # This is for the music
import pygame, time, random

mixer.init()
mixer.music.load(PlayMusic.introMusic)
mixer.music.play(-1) # The -1 allows the track to play on repeat.
pygame.display.set_caption("Tombstone Rising")


def title():
    print('''                                      (                        
      *   )              )       )                 )\ )                     
    ` )  /(      )    ( /(    ( /(            (   (()/((    (        (  (   
     ( )(_)|    (     )\())(  )\())(   (     ))\   /(_))\ ( )\  (    )\))(  
    (_(_()))\   )\  '((_)\ )\(_))/ )\  )\ ) /((_) (_))((_))((_) )\ )((_))\  
    |_   _((_)_((_)) | |(_|(_) |_ ((_)_(_/((_))   | _ \(_|(_|_)_(_/( (()(_) 
      | |/ _ \ '  \()| '_ (_-<  _/ _ \ ' \)) -_)  |   /| (_-< | ' \)) _` |  
      |_|\___/_|_|_| |_.__/__/\__\___/_||_|\___|  |_|_\|_/__/_|_||_|\__, |  
                                                                    |___/ ''')
title()



life = int(5) # The choices you make will afect this number and how the story ends.
name = input("What is your name?\n") #Create a namm for your character
animate_text = TextAnimations() # This allows me to call in the class moducle
list= ['Ghast','Gug','Nyarlathotep','Mi-go','Shoggoth','Dagon'] #List of monsters from HP Lovecraft.
monster = None

def intro(name):
    line_1 = str("A bead of sweat rolls down your back as you are jolted awake from your sleep!\n")
    animate_text.fast(line_1)
intro(name)

def body(name):
    line_2 = str(name+"! I need for you to wake up.\n"\
               "Something has happened! You need to get out of the ...\n"
               "NO..\n"
               "NO...\n"
               "NO... AHHHHHHHHHHH!\n")
    animate_text.fast(line_2)
body(name)

def cross_road(name, life):

    light = int(1)
    shield = int(2)
    bat = int(3)

    line_3 = str(name+" in a panic you look around your room in an attempt to understand what is happening. \n"
          "You see the following items.\n"
          "A flash light,\n"
          "a bat,\n"
          "and a prop shield\n")
    animate_text.fast(line_3)
    decision_1 = input(str("Which shall you chose?"))
    if decision_1.lower() == str("flash light"):
        print("You reach out and pick up the flash light.\n"
              "Fear controls your every waking moment and you suffer a -" +str(light)+" to your wisdom.")
        lifeTotal = life - light
    elif decision_1.lower() == str("bat"):
        print("You decided to take the bat.\n"
              "Knowing that nothing will stop you from getting out.\n"
              "You have earned a +"+str(bat)+" to your strength.")
        lifeTotal = life + bat
    elif decision_1.lower() == str("shield"):
        print("The shield was your first chose.\n"
              "It will take a little more then a dark room to scare you.\n"
              "You have gained a +"+str(shield)+" to your defense.")
        lifeTotal = life + shield
    else:
        print("You chose not to take anything.\n"
              "You are either very brave or very stupid!")
        lifeTotal = life + random.randint(-5,5)
    return lifeTotal
lifeTotal = cross_road(name,life)


def BedroomDoor(name, lifeTotal):
    global monster
    monster = random.choice(list)
    door = "You reach out and open the door. That is when you see a "+str(monster)+"!\n"
    #decision_2 = input(str("Do you choice to run or fight:"))
    if lifeTotal == int(4): # Flash Light
        line_5 = str(name+" you turn on the flash light and the first thing you see is ... \n"
                     "Nothing! Your room is just as it was before you went to sleep.\n"
                     "This can't be right you think to yourself as you go to the bedroom door\n"
                     "You reach out and open the door, but is it so dark you can not see anything,\n"
                     "so you turn on the flash light!\n")
        animate_text.fast(line_5)
    elif lifeTotal == int(7): # Shield
        line_5 = str(name+" you decide to strap the shield to your arm just before \n"
                          "placing your bear feet on the ground and you make your way to the door of the bedroom\n"
                     +door)
        animate_text.fast(line_5)
    elif lifeTotal == int(8): # Bat
        line_5 = str(name+ " your grip around the handle of the bat becomes tighter as you make your way \n"
                     "to the bedroom door.\n"
                     +door)
        animate_text.fast(line_5)
    else:
        line_5 = str(name+ " you place your bear feet on the follow and head directly to the bedroom door.\n"
                     +door)
        animate_text.fast(line_5)
BedroomDoor(name, lifeTotal)

def Hallway(name, lifeTotal, monster):
    if lifeTotal == int(4): # Flash Light
        pygame.mixer.stop()
        time.sleep(2)
        mixer.music.load(PlayMusic.doorMusic)
        mixer.music.play()
        line_6 = str(name+", you walk into the dark hallway.\n"
                        "The flash light you grabbed earlier does not offer any assistants\n"
                     "After several steps forward you begin to feel like something has been watching you this whole time.\n"
                     "Suddenly you hear "+name+" you made it out! I am so glad. I thought they had gotten to you\n"
                     "The voice that you hear sounds a like your fathers but there is something odd about it.\n")
        animate_text.fast(line_6)
        line_7 = input(str("Do you run away or search for the source of the noise?"))
        if line_7.lower() == str("run"):
            line_7 = str("RUN, RUN, RUN!!!!\n"
                         "That is what you keep telling yourself but you legs feel as if they have weights attached to them,\n"
                         """you suddenly hear a whisper saying "bring them to me" as somthing cold and slimy grabs you!\n""")
            animate_text.fast(line_7)
        elif line_7.lower() == str("search"):
            line_7 = str(name+" you step out into the hallway and into the darkness.\n"
                         "Your heart is beating so loud that you swear that you can hear it outside of your chest,\n"
                         """Everything is ok!" that is what you keep telling yourself but you know you know that something\n"""
                         "is about to happen. You look behind you when suddenly you are taken away by a "+str(monster)+"!\n")
            mixer.music.load(PlayMusic.fear)
            mixer.music.play()
        else:
            line_7 = str(name+" your blood runs cold with fear as you are unable to move.\n"
                              "The "+str(monster)+", slowly starts to appear from the wall behind you\n"
                                                  "as the release an ear shattering scream you lose consciousness.\n")
        animate_text.fast(line_7)
        mixer.init()
        mixer.music.load(PlayMusic.MercyMusic)
        mixer.music.play()

    elif lifeTotal == int(7): # Shield
        decision_3 = input(str("How do you get past the "+str(monster)+"!\n"
                  "Do you push it back with your shield\n"
                  "or do you jump out the bedroom window?\n"))
        line_6 = str(name + " you are staring a " + str(monster) + " in what you think is a face!\n")
        animate_text.fast(line_6)
        if decision_3.lower() == str("push"):
            line_6 = str("You raise the shield to cover your face and run toward the "+str(monster)+"\n"
                         "and will all your might you attempt to push though\n"
                         "you pass though with no resistance as if there was nothing there!\n"
                         "Then a cold numbing pain starts to sit in as if your entire body refuses to move,\n"
                         "your vision starts to blur and you the floor starts to get closer as you collapse.")
            animate_text.fast(line_6)
        elif decision_3.lower() == str("jump"):
            line_6 = str(name+" the "+str(monster)+" has you terrified and you decide that going out your window would be easier.\n"
                         "You raise the shield to protect your face and you run for the window.\n"
                         "Upon impact with the glass you immediately regret your decision and everything goes black.\n")
            animate_text.fast(line_6)
            lifeTotal = lifeTotal - lifeTotal  # Since lifeTotal is 0 you will go directly to the end of the story.
        else:
            line_6 = str("You did not make a choice and the "+str(monster)+"looks as you blankly as you a shadow from behind\n"
                  "starts to drag you down into the under world.")
            lifeTotal = lifeTotal - lifeTotal  # Your where taken by the shadow. To the end of the story you go.
            animate_text.fast(line_6)
            mixer.music.load(PlayMusic.screamMusic)
            mixer.music.play()
    elif lifeTotal == int(8): # Bat
        line_6 = str(name+", you close your eys, refusing to look at the "+str(monster)+" that is when you\n"
                    " swing the bat with all your might."
                     "with a loud WACK! you make contact with what you hope is the monster in front of you!\n"
                     "When you open your eyes what you see is an unspeakable site.")
        animate_text.fast(line_6)
    else: # Random
        pass
Hallway(name, lifeTotal, monster)

def EndOfStory(name):
    mixer.init()
    mixer.music.load(PlayMusic.hell)
    mixer.music.play(-1)
    finalLine = str(name+", the sound of fire wakes you, but what you see next is unspeakable. You are staring at a \n"
                         "monster of vaguely anthropoid outline, but with an octopus-like head whose face was a mass \n"
                         "of feelers, a scaly, rubbery-looking body, prodigious claws on hind and fore feet, and long, \n"
                         "narrow wings behind... \n")
EndOfStory(name)
