'''
This class was created to be a library for all the MP3's that are need in the game.
'''

class PlayMusic:
    introMusic = '.\music\Creeping_Death.mp3'
    doorMusic = '.\music\Creaking_Door_Spooky.mp3'
    screamMusic = '.\music\Scary_Scream.mp3'
    MercyMusic = '.\music\Mercy.mp3'
    hell = '.\music\hell.mp3'
    fear = '.\LurkingFear.mp3'
