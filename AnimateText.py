'''
Try to create a Class to anime text
1) Control the speed of the text
'''
import sys
from time import sleep


class TextAnimations:

    def __init__(self):
        self.speed = 0.5

    def slow(self, line):
        self.line = line
        self.speed = 1
        for x in self.line:
            print(x, end='')
            sys.stdout.flush()
            sleep(self.speed)

    def normal(self, line):
        self.line = line
        for x in self.line:
            print(x, end='')
            sys.stdout.flush()
            sleep(self.speed)

    def fast(self, line):
        self.line = line
        self.speed = 0.1
        for x in self.line:
            print(x, end='')
            sys.stdout.flush()
            sleep(self.speed)


    def custom(self, line, speed):
        self.line = line
        self.speed = speed
        for x in self.line:
            print(x, end='')
            sys.stdout.flush()
            sleep(self.speed)